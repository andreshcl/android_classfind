package android.moviles.epn.classfind

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    //firebase
    private lateinit var firebaseAuth: FirebaseAuth

    //google
    val RC_SIGN_IN: Int =1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions

    //email-correo
    lateinit var emailET: EditText
    lateinit var passwordET: EditText

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, LoginActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        emailET = findViewById(R.id.txtEmail)
        passwordET = findViewById(R.id.txtPassword)

        configureGoogleSignIn()
        setupUI()

        firebaseAuth = FirebaseAuth.getInstance()

    }


    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser

        if (user != null) {
            startActivity( MenuActivity.getLaunchIntent(this))
            finish()
        }
    }


    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun setupUI() {
        btn_Google.setOnClickListener {
            signInGoogle()
        }
        btn_sign_in_mail_pass.setOnClickListener{
            signInEmail()
        }



    }

    private fun signInGoogle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                //firebaseAuthWithGoogle(account)
                account?.let { firebaseAuthWithGoogle(it) } //ver
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
                Log.e("Tag", "Error: "+e.toString())
                Log.i("Tag2", e.toString())
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {

                startActivity(MenuActivity.getLaunchIntent(this))
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun signInEmail(){
        var email = emailET.text.toString()
        val password = passwordET.text.toString()
        if (!email.isEmpty() && !password.isEmpty()){
            emailPasswordAuthentication(email,password)
        }else{
            Toast.makeText(this, "Please fill up the credentials :o", Toast.LENGTH_LONG).show()
        }
    }

    private fun emailPasswordAuthentication(email: String, password:String ){
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener {
            if (it.isSuccessful){
                startActivity(MenuActivity.getLaunchIntent(this))
            }else{
                Toast.makeText(this, "No registered :c", Toast.LENGTH_LONG).show()
                emailPasswordRegister(email,password)
            }
        }
    }

    private fun emailPasswordRegister(email: String, password:String){
        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, OnCompleteListener {
            if(it.isSuccessful){
                val user = firebaseAuth.currentUser
                val uid = user!!.uid

                val myDatabase = FirebaseDatabase.getInstance().getReference("Users")
                myDatabase.child(uid).child("User").setValue(user).addOnCompleteListener {
                    Toast.makeText(this, "registered c:", Toast.LENGTH_LONG).show()
                }
                startActivity(MenuActivity.getLaunchIntent(this))

            }else{
                Toast.makeText(this,"email register failed :c",Toast.LENGTH_LONG).show()
            }
        })
    }
}
